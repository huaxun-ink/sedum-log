package ink.huaxun.log.controller;

import ink.huaxun.authority.annotation.RequiresPermissions;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.enumeration.OrderEnum;
import ink.huaxun.core.vo.Page;
import ink.huaxun.core.vo.Result;
import ink.huaxun.core.vo.Sort;
import ink.huaxun.log.entity.SysLog;
import ink.huaxun.log.mapper.SysLogMapper;
import ink.huaxun.log.service.SysLogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaogang
 * @description 日志
 * @date 2023/3/6 17:33
 */
@RestController
@RequestMapping("/sys/log")
public class SysLogController extends BaseController<SysLogService, SysLogMapper, SysLog> {

    @RequiresPermissions
    @GetMapping
    @Override
    public Result list(SysLog log, Page page, Sort sort) {
        sort.setColumn("createTime");
        sort.setOrder(OrderEnum.DESC);
        return super.list(log, page, sort);
    }

}
