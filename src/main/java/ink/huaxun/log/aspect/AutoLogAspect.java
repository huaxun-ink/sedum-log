package ink.huaxun.log.aspect;

import ink.huaxun.log.entity.SysLog;
import ink.huaxun.log.service.SysLogService;
import ink.huaxun.log.annotation.IgnoreLog;
import ink.huaxun.util.HttpServletUtil;
import ink.huaxun.util.IpUtil;
import ink.huaxun.util.JsonUtil;
import ink.huaxun.util.UserUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Locale;

/**
 * @author zhubo
 * @description 系统日志，切面处理类
 * @date 2020/6/3 11:02
 */
@Aspect
@Component
public class AutoLogAspect {

    @Resource
    private SysLogService sysLogService;

    @Pointcut("execution(* ink.huaxun.*..controller..*.*(..))")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object doBeforeMethod(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        IgnoreLog ignoreLog = method.getAnnotation(IgnoreLog.class);
        long beginTime = System.currentTimeMillis();
        Object[] args = point.getArgs();
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ServletRequest || args[i] instanceof ServletResponse || args[i] instanceof MultipartFile) {
                //ServletRequest不能序列化，从入参里排除，否则报异常：java.lang.IllegalStateException: It is illegal to call this method if the current request is not in asynchronous mode (i.e. isAsyncStarted() returns false)
                //ServletResponse不能序列化 从入参里排除，否则报异常：java.lang.IllegalStateException: getOutputStream() has already been called for this response
                args[i] = null;
            }
        }
        String params = JsonUtil.toString(args);
        //执行方法
        Object result = point.proceed();
        if (ignoreLog != null && ignoreLog.value()) {
            return result;
        }
        long endTime = System.currentTimeMillis();
        saveSysLog(point, params, endTime - beginTime);
        return result;
    }

    private void saveSysLog(JoinPoint joinPoint, String params, Long time) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        HttpServletRequest request = HttpServletUtil.getRequest();
        SysLog sysLog = new SysLog();
        //设置IP地址
        if (request != null) {
            sysLog.setRequestUrl(request.getRequestURI());
            sysLog.setIp(IpUtil.getIpAddress(request));
        }
        //注解上的描述,操作日志内容
        sysLog.setRequestType(request != null ? request.getMethod().toLowerCase(Locale.ROOT) : null);
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLog.setMethod(className + "." + methodName + "()");
        sysLog.setRequestParam(params);
        sysLog.setUserId(String.valueOf(UserUtil.getId()));
        sysLog.setCostTime(time);
        sysLog.setCreateTime(new Date());
        sysLogService.save(sysLog);
    }

}
