package ink.huaxun.log.mapper;

import ink.huaxun.core.mapper.BaseMapper;
import ink.huaxun.log.entity.SysLog;
import org.springframework.stereotype.Repository;

/**
 * 系统日志表 Mapper 接口
 *
 * @Author zhubo
 * @since 2018-12-26
 */
@Repository
public interface SysLogMapper extends BaseMapper<SysLog> {

}
