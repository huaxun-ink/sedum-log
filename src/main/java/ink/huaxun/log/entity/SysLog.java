package ink.huaxun.log.entity;

import ink.huaxun.core.entity.BaseEntity;
import lombok.Data;

/**
 * 系统日志表
 *
 * @Author zhubo
 * @since 2020-6-3
 */
@Data
public class SysLog extends BaseEntity {

    private static final long serialVersionUID = 2958814520630652923L;

    /**
     * 耗时
     */
    private Long costTime;

    /**
     * IP
     */
    private String ip;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 请求类型
     */
    private String requestType;

    /**
     * 请求路径
     */
    private String requestUrl;
    /**
     * 请求方法
     */
    private String method;

    /**
     * 操作人用户账户
     */
    private String userId;
    /**
     * 操作详细日志
     */
    private String logContent;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 操作类型
     */
    private String operateType;


}
