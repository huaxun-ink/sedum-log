package ink.huaxun.log.service;

import ink.huaxun.core.service.BaseService;
import ink.huaxun.log.entity.SysLog;
import ink.huaxun.log.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

/**
 * @author zhaogang
 * @description 日志
 * @date 2023/3/7 9:42
 */
@Service
public class SysLogService extends BaseService<SysLogMapper, SysLog> {

}
